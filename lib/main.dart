import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSU Trang',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.account_circle_sharp),
            title: Text('My First App'),
            actions: [
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.add_alarm),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.add_alert),
              ),
            ],
          ),
          body: Center(
              child: Column(
                children: [
                  //Image.asset('assets/IMG_20220718_160420.jpg',
                    //width: 300,
                    //height: 300,
                    //fit: BoxFit.cover,
                  //),

                  CircleAvatar(
                    backgroundImage: AssetImage('assets/IMG_20220718_160420.jpg'),
                    radius: 140,
                  ),
                  Text(
                    'Natthanon Songsrijan',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
          )
      ),
    );
  }
}
